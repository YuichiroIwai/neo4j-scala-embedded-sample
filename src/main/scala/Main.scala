import java.io.File

import org.neo4j.graphdb.factory.GraphDatabaseFactory
import org.neo4j.graphdb.{GraphDatabaseService, Label, RelationshipType}

import scala.collection.JavaConverters._

object Main extends App {
  // 埋め込みDB作成
  // カレントディレクトリにdataディレクトリが掘られる
  val db = new GraphDatabaseFactory().newEmbeddedDatabase(new File("data"))

  // データを全部消す
  cleanup(db)

  // ノードを10個作る
  for (i <- 1 to 10) {
    // トランザクション開始
    val tx = db.beginTx()

    // ラベル指定でノード取得
    val nodes = db.findNodes(Label.label("Node"))

    // 新しいノードを作成
    val node = db.createNode(Label.label("Node"))

    // ノードにプロパティをセット
    node.setProperty("id", i)

    // 全ノードとリレーションを作成
    nodes.asScala.foreach { n =>
      node.createRelationshipTo(n, RelationshipType.withName("Edge"))
      n.createRelationshipTo(node, RelationshipType.withName("Edge"))
    }

    // トランザクション成功
    tx.success()

    // トランザクションをクローズ
    tx.close()
    println(s"transaction closed: $i")
  }

  // 全ノードを出力
  showAllNodes(db)

  // DBをシャットダウン
  db.shutdown()

  def showAllNodes(db: GraphDatabaseService) = {
    // お約束
    val tx = db.beginTx()

    // ラベル指定でノードを取得
    db.findNodes(Label.label("Node")).asScala.foreach { n =>
      // プロパティをMapで出力
      print(n.getAllProperties.asScala)

      // リレーションの数
      println(n.getRelationships().asScala.size)
    }
    tx.close()
  }

  def cleanup(db: GraphDatabaseService): Unit = {
    println("start cleanup")

    // お約束
    val tx = db.beginTx()

    // cypherで全ノードとリレーションをマッチさせて消す
    db.execute("match (n:Node)-[r]-() delete n, r")

    tx.success()
    tx.close()
    println("end cleanup")
  }

}
