name := "neo4j-scala-embedded-sample"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "org.neo4j" % "neo4j" % "3.1.0-M10"
)
